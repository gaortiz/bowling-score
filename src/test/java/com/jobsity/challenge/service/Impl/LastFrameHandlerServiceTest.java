package com.jobsity.challenge.service.Impl;

import com.jobsity.challenge.service.impl.LastFrameHandlerService;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LastFrameHandlerServiceTest {

    private final LastFrameHandlerService lastFrameHandlerService = new LastFrameHandlerService();

    @Test
    public void setLastFramePinFalls_allStrikes_returnPerfectScore() {

        Map<Integer, List<String>> pinFalls = new HashMap<>();

        String actualShot = "X";
        String nextShot = "X";
        String afterNextShot = "X";
        int actualShotValue = 10;
        int nextShotValue = 10;
        int afterNextShotValue = 10;

        final Map<Integer, List<String>> expectedLastFramPinFall =
                Stream.of(new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("X", "X", "X")))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        lastFrameHandlerService.setLastFramePinFalls(pinFalls, actualShot, nextShot, afterNextShot,
                actualShotValue, nextShotValue, afterNextShotValue);

        Assert.assertEquals(expectedLastFramPinFall, pinFalls);
    }

    @Test
    public void setLastFramePinFalls_twoPinFallsLessThan10_returnTwoScores() {

        Map<Integer, List<String>> pinFalls = new HashMap<>();

        String actualShot = "1";
        String nextShot = "8";
        String afterNextShot = "0";
        int actualShotValue = 1;
        int nextShotValue = 8;
        int afterNextShotValue = 0;

        final Map<Integer, List<String>> expectedLastFramPinFall =
                Stream.of(new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("1", "8")))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        lastFrameHandlerService.setLastFramePinFalls(pinFalls, actualShot, nextShot, afterNextShot,
                actualShotValue, nextShotValue, afterNextShotValue);

        Assert.assertEquals(expectedLastFramPinFall, pinFalls);
    }

    @Test
    public void setLastFramePinFalls_spareOnSecondAttempt_returnSlashInSecondAttempt() {

        Map<Integer, List<String>> pinFalls = new HashMap<>();

        String actualShot = "5";
        String nextShot = "5";
        String afterNextShot = "5";
        int actualShotValue = 5;
        int nextShotValue = 5;
        int afterNextShotValue = 5;

        final Map<Integer, List<String>> expectedLastFramPinFall =
                Stream.of(new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("5", "/", "5")))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        lastFrameHandlerService.setLastFramePinFalls(pinFalls, actualShot, nextShot, afterNextShot,
                actualShotValue, nextShotValue, afterNextShotValue);

        Assert.assertEquals(expectedLastFramPinFall, pinFalls);
    }

    @Test
    public void setLastFramePinFalls_spareOnLastAttempt_returnSlashInLastAttempt() {

        Map<Integer, List<String>> pinFalls = new HashMap<>();

        String actualShot = "X";
        String nextShot = "0";
        String afterNextShot = "X";
        int actualShotValue = 10;
        int nextShotValue = 0;
        int afterNextShotValue = 10;

        final Map<Integer, List<String>> expectedLastFramPinFall =
                Stream.of(new AbstractMap.SimpleImmutableEntry<>(10, Arrays.asList("X", "0", "/")))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        lastFrameHandlerService.setLastFramePinFalls(pinFalls, actualShot, nextShot, afterNextShot,
                actualShotValue, nextShotValue, afterNextShotValue);

        Assert.assertEquals(expectedLastFramPinFall, pinFalls);
    }

}
